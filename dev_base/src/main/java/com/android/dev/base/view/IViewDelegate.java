package com.android.dev.base.view;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created with Android Studio.
 * Project:Android_Dev
 * Title:IViewDelegate
 * Description:视图层代理协议接口
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/12
 * Time:11:04
 * Version 1.0
 */
public interface IViewDelegate {
    void create (LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState);

    int getOptionsMenuId ();

    Toolbar getToolbar();

    View getRootView();

    void initWidget();
}
