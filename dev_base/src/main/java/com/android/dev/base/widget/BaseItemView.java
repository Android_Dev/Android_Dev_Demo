/**
 * Title:BaseItemView
 * Description:TODO
 * Copyright:Copyright (c) 2014
 * Company:
 * Author:maple
 * Date:2014/8/10
 * Version 1.0
 */
package com.android.dev.base.widget;

import android.view.LayoutInflater;
import android.view.View;

/**
 * Created with IntelliJ IDEA..
 * Title:BaseItemView
 * Description:
 * Author:maple
 * Date:2014/8/10
 * Time:18:41
 */
public abstract class BaseItemView<T, VH extends BaseItemView.ViewHolder> implements IItemView {
    private T data;

    public BaseItemView(T data) {
        this.data = data;
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        VH holder;
        if (convertView == null) {
            convertView = inflater.inflate(getLayoutId(), null);
            holder = getBaseViewHolder(convertView);
            initHolderView(holder, convertView);
            convertView.setTag(holder);
        } else {
            holder = (VH) convertView.getTag();
        }
        bindItemViews(holder, getData());
        return convertView;
    }

    /**
     * 获取ItemView对应的object
     *
     * @return
     */
    public T getData() {
        return data;
    }

    /**
     * 获取ViewHolder对象
     *
     * @return
     */
    protected abstract VH getBaseViewHolder(View view);

    /**
     * ItemView布局资源文件Id
     *
     * @return
     */
    protected abstract int getLayoutId();

    /***
     * 绑定ItemView数据
     *
     * @param holder
     * @param t
     */
    protected abstract void bindItemViews(VH holder, T t);

    /**
     * 初始化ViewHolder
     *
     * @param holder
     * @param convertView
     */
    protected abstract void initHolderView(VH holder, View convertView);

    public static class ViewHolder {
        public ViewHolder(View view) {

        }
    }
}
