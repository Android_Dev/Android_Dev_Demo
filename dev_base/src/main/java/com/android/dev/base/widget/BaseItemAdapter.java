/**
 * Title:BaseItemAdapter
 * Description:TODO
 * Copyright:Copyright (c) 2014
 * Company:
 * Author:maple
 * Date:2014/8/10
 * Version 1.0
 */
package com.android.dev.base.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created with IntelliJ IDEA..
 * Title:BaseItemAdapter
 * Description:
 * Author:maple
 * Date:2014/8/10
 * Time:18:46
 */
public abstract class BaseItemAdapter extends ArrayAdapter<IItemView> {

    private List<IItemView> mList;
    private Context mCtx;
    private LayoutInflater mInflater;

    public BaseItemAdapter(Context context, List<IItemView> list) {
        super(context, 0, list);
        mCtx = context;
        mList = list;
        mInflater = LayoutInflater.from(mCtx);
    }

    @Override
    public int getViewTypeCount() {
        return getRowTypeCount();
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getViewType();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = getItem(position).getView(mInflater, convertView);
        return view;
    }

    public void add(IItemView itemView) {
        if (null != mList) {
            mList.add(itemView);
            notifyDataSetChanged();
        }
    }

    public void add(List<IItemView> list) {
        int size = null == list ? 0 : list.size();
        if (size < 1) {
            return;
        }
        if (null == mList) {
            mList = list;
            notifyDataSetChanged();
            return;
        }
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void addAll(int index, List<IItemView> list) {
        int size = null == list ? 0 : list.size();
        if (size < 1) {
            return;
        }
        if (mList != null) {
            mList.addAll(index, list);
            notifyDataSetChanged();
            return;
        }
        mList = list;
        notifyDataSetChanged();
    }

    public void clear() {
        if (null != mList) {
            mList.clear();
            notifyDataSetChanged();
        }
    }

    public List<IItemView> getItemViewList() {
        return mList;
    }

    protected LayoutInflater getLayoutInflater() {
        return mInflater;
    }

    protected abstract int getRowTypeCount();

}
