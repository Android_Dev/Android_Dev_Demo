package com.android.dev.base.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.dev.base.view.IViewDelegate;


/**
 * Created with Android Studio.
 * Project:AndroidCommon
 * Title:FragmentPresenter
 * Description: Fragment Presenter 基类
 * Copyright:Copyright (c) 2014
 * Company:中国平安健康保险有限公司
 * Author:mahuiyang
 * Date:15/12/29
 * Time:14:52
 * Version 1.0
 */
public abstract class FragmentPresenter <T extends IViewDelegate> extends Fragment {


    protected T mViewDelegate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDelegate();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (null != mViewDelegate) {
            mViewDelegate.create(inflater, container, savedInstanceState);
            return mViewDelegate.getRootView();
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (null != mViewDelegate) {
            mViewDelegate.initWidget();
            bindEventListener();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mViewDelegate = null;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        initDelegate();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        int menuId = null == mViewDelegate ? 0 : mViewDelegate.getOptionsMenuId();
        if (0 != menuId) {
            inflater.inflate(menuId,menu);
        }
    }

    private void initDelegate () {
        if (null == mViewDelegate) {
            try {
                mViewDelegate = getDelegateClass().newInstance();
            } catch (java.lang.InstantiationException e) {
                throw new RuntimeException("create IViewDelegate error");
            } catch (IllegalAccessException e) {
                throw new RuntimeException("create IViewDelegate error");
            }
        }
    }

    protected void bindEventListener(){}

    protected abstract Class<T> getDelegateClass();
}
