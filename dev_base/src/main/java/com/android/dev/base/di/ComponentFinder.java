package com.android.dev.base.di;

import android.content.Context;

/**
 * Created with Android Studio.
 * Project:Android_Dev_Demo
 * Title:ComponentFinder
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/27
 * Time:14:00
 * Version 1.0
 */
public class ComponentFinder {

    private ComponentFinder(){}

    public static <C> C findActivityComponent (Context ctx) {
        return ((HasComponent<C>)ctx).getComponent();
    }
}
