package com.android.dev.base.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created with Android Studio.
 * Project:Android_Dev
 * Title:BaseViewDelegate
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/12
 * Time:11:05
 * Version 1.0
 */
public abstract class BaseViewDelegate implements IViewDelegate {

    protected final SparseArray<View> mViews = new SparseArray<>();

    protected View mRootView;

    @Override
    public void create(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutId = getLayoutId();
        mRootView = inflater.inflate(layoutId,container,false);
    }

    @Override
    public View getRootView() {
        return mRootView;
    }

    private <T extends View> T bindView (int id) {
        T view = (T) mViews.get(id);
        if (null == view) {
            view = (T) mRootView.findViewById(id);
            mViews.put(id,view);
        }
        return view;
    }

    public <T extends View> T get (int id) {
        return bindView(id);
    }

    public <T extends Activity> T getActivity () {
        if (null == mRootView) {
            return null;
        }
        Context ctx = mRootView.getContext();
        if (ctx instanceof Activity) {
            return (T) ctx;
        }
        return null;
    }

    public void setOnClickListener (View.OnClickListener listener,int... ids) {
        if (null == ids) {
            return;
        }
        for (int id : ids) {
            View view = get(id);
            if (null != view) {
                view.setOnClickListener(listener);
            }
        }
    }

    protected abstract int getLayoutId ();
}
