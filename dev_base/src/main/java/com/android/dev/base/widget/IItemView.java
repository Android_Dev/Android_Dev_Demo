/**
 * Title: IItemView
 * Description:
 * Copyright: Copyright (c) 2014
 * Company:
 * Author maple
 * Date 2014/6/6
 * Version 1.0
 */
package com.android.dev.base.widget;

import android.view.LayoutInflater;
import android.view.View;

/**
 * Created with IntelliJ IDEA.
 * Title:IItemView
 * Description:消息类型布局接口
 * Author:maple
 * Date:2014/6/6
 * Time:13:10
 */
public interface IItemView {
    /**消息布局类型*/
    int getViewType();

    /***
     * 消息布局
     * @param inflater
     * @param convertView
     * @return
     */
    View getView(LayoutInflater inflater, View convertView);
}
