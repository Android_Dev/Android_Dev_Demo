package com.android.dev.base.di;

/**
 * Created with Android Studio.
 * Project:Android_Dev_Demo
 * Title:HasComponent
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/27
 * Time:13:59
 * Version 1.0
 */
public interface HasComponent<C> {

    C getComponent();
}
