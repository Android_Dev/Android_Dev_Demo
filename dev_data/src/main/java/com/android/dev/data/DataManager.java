package com.android.dev.data;

import android.content.Context;

import com.android.dev.data.api.ApiHelper;
import com.android.dev.data.api.model.PostInfoResp;
import com.android.dev.data.db.DBHelper;
import com.android.dev.data.di.qualifier.ApplicationContext;

import javax.inject.Inject;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by wangyouguo on 16/1/11.
 */
public class DataManager {
    Context mAppContext;
    ApiHelper mApiHelper;
    DBHelper mDBHelper;
    EventPosterHelper mEventPosterHelper;

    @Inject
    public DataManager(@ApplicationContext Context context, ApiHelper apiHelper, DBHelper dbHelper, EventPosterHelper eventPosterHelper) {
        mAppContext = context;
        mApiHelper = apiHelper;
        mDBHelper = dbHelper;
        mEventPosterHelper = eventPosterHelper;
    }

    public Observable<PostInfoResp> getCategoryDataList(final String type, final int pageSize, final int pageIndex) {
        return mApiHelper.getApiMainService()
                .getCategoryDataList(type, pageSize, pageIndex)
                .subscribeOn(Schedulers.io())
                .map(new Func1<PostInfoResp, PostInfoResp>() {
                    @Override
                    public PostInfoResp call(PostInfoResp postInfoResp) {
                        mDBHelper.insertOrReplaceInTx(mDBHelper.getPostInfoDao(), postInfoResp.results);
                        return postInfoResp;
                    }
                });
    }

    public Observable<PostInfoResp> getRandomCategoryDataList(String type, int pageSize) {
        return mApiHelper.getApiMainService().getRandomCategoryDataList(type, pageSize)
                .subscribeOn(Schedulers.io())
                .map(new Func1<PostInfoResp, PostInfoResp>() {
                    @Override
                    public PostInfoResp call(PostInfoResp postInfoResp) {
                        mDBHelper.insertOrReplaceInTx(mDBHelper.getPostInfoDao(), postInfoResp.results);
                        return postInfoResp;
                    }
                });
    }

    public Observable<PostInfoResp> getDayData(int year, int month, int day) {
        return mApiHelper.getApiListService().getDayData(year, month, day)
                .subscribeOn(Schedulers.io())
                .map(new Func1<PostInfoResp, PostInfoResp>() {
                    @Override
                    public PostInfoResp call(PostInfoResp postInfoResp) {
                        mDBHelper.insertOrReplaceInTx(mDBHelper.getPostInfoDao(), postInfoResp.results);
                        return postInfoResp;
                    }
                });
    }
}
