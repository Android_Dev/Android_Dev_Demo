package com.android.dev.data.di.module;

import android.app.Application;
import android.content.Context;

import com.android.dev.data.di.qualifier.ApplicationContext;

import dagger.Module;
import dagger.Provides;

/**
 * Created by wangyouguo on 16/1/18.
 */
@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

}
