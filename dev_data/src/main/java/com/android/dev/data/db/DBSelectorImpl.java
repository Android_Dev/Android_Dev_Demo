package com.android.dev.data.db;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created with Android Studio.
 * Project:Android_Dev
 * Title:DBSelectorImpl
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/11
 * Time:16:20
 * Version 1.0
 */
public class DBSelectorImpl implements IDBSelector<PostInfo> {

    @Inject
    public DBSelectorImpl(){

    }

    @Inject
    DBHelper mDBHelper;

    @Override
    public Observable<List<PostInfo>> query(final int index, final int size) {
        return mDBHelper.makeObservable(new Callable<List<PostInfo>>() {
            @Override
            public List<PostInfo> call() throws Exception {
                return mDBHelper.query(mDBHelper.getPostInfoDao(), index, size);
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<PostInfo> query(final long id) {
        return mDBHelper.makeObservable(new Callable<PostInfo>() {
            @Override
            public PostInfo call() throws Exception {
                return (PostInfo) mDBHelper.load(mDBHelper.getPostInfoDao(), id);
            }
        }).subscribeOn(Schedulers.io());
    }
}
