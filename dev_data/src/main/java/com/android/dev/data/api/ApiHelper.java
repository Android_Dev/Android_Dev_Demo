package com.android.dev.data.api;

import android.content.Context;

import com.android.dev.data.BuildConfig;
import com.android.dev.data.api.service.ApiListService;
import com.android.dev.data.api.service.ApiMainService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.ConcurrentMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.RxJavaCallAdapterFactory;


/**
 * Created by wangyouguo on 16/1/11.
 */
public class ApiHelper {
    public static final String BASE_URL = "http://gank.avosapps.com/api/";
    private Retrofit mRetrofit;
    private ApiMainService mApiMainService;
    private ApiListService mApiListService;

    @Inject
    public ApiHelper(Retrofit retrofit, ApiMainService apiMainService, ApiListService apiListService) {
        mRetrofit = retrofit;
        mApiMainService = apiMainService;
        mApiListService = apiListService;
    }

    public ApiMainService getApiMainService() {
        if (mApiMainService == null) {
            mApiMainService = getApiService(ApiMainService.class);
        }
        return mApiMainService;
    }

    public ApiListService getApiListService() {
        if (mApiListService == null) {
            mApiListService = getApiService(ApiListService.class);
        }
        return mApiListService;
    }

    public <T> T getApiService(Class<T> serviceType) {
        return ApiFactory.factory(mRetrofit, serviceType);
    }


}
