package com.android.dev.data.di;

import android.content.Context;

import com.android.dev.data.api.ApiFactory;
import com.android.dev.data.api.ApiHelper;
import com.android.dev.data.api.service.ApiListService;
import com.android.dev.data.api.service.ApiMainService;
import com.android.dev.data.db.DBHelper;
import com.android.dev.data.db.DBSelectorImpl;
import com.android.dev.data.db.IDBSelector;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;
import retrofit2.Retrofit;

/**
 * Created by wangyouguo on 16/1/12.
 */
@Singleton
@Module
public class DataModule {
    private Context mAppContent;

    public DataModule(Context context) {
        mAppContent = context.getApplicationContext();
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit() {
        return ApiFactory.makeRetrofit(mAppContent, ApiHelper.BASE_URL);
    }

    @Singleton
    @Provides
    ApiMainService provideApiMainService(Retrofit retrofit) {
        return ApiFactory.factory(retrofit, ApiMainService.class);
    }

    @Singleton
    @Provides
    ApiListService provideApiListService(Retrofit retrofit) {
        return ApiFactory.factory(retrofit, ApiListService.class);
    }


    @Provides
    @Singleton
    DBHelper provideDBHelper() {
        return new DBHelper(mAppContent);
    }

    @Provides
    @Singleton
    EventBus provideEventBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Singleton
    IDBSelector provideIDBSelector(DBSelectorImpl dbSelector){
        return dbSelector;
    }

    @Provides
    @Singleton
    Gson provideGson () {
        GsonBuilder builder = new GsonBuilder();
        builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return builder.create();
    }
}
