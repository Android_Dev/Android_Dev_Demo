package com.android.dev.data;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import com.android.dev.data.di.component.ActivityComponent;
import com.android.dev.data.di.component.ApplicationComponent;
import com.android.dev.data.di.module.ActivityModule;

/**
 * Created by wangyouguo on 16/1/18.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

}
