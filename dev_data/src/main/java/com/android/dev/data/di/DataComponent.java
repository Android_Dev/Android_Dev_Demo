package com.android.dev.data.di;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by wangyouguo on 16/1/12.
 */
@Singleton
@Component(modules = DataModule.class)
public interface DataComponent {
    //void inject();
}
