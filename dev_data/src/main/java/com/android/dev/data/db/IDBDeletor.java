package com.android.dev.data.db;

import rx.Observable;

/**
 * Created with Android Studio.
 * Project:Android_Dev
 * Title:IDBDeletor
 * Description:数据库删除操作接口
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/11
 * Time:16:52
 * Version 1.0
 */
public interface IDBDeletor {

    /**
     * 删除指定key的帖子
     * @param key   帖子主键
     * @return
     */
    Observable<Void> deletePostInfo (long key);

    /**
     * 批量删除帖子
     * @param keys  帖子主键集合
     * @return
     */
    Observable<Void> delete (Iterable<PostInfo> keys);
}
