package com.android.dev.data.db;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;

/**
 * Created with Android Studio.
 * Project:Android_Dev
 * Title:DBUpdaterImpl
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/11
 * Time:17:01
 * Version 1.0
 */
public class DBUpdaterImpl implements IDBUpdater {

    @Inject
    DBHelper mDBHelper;

    @Override
    public Observable<Long> insertOrReplace(final PostInfo obj) {
        return Observable.create(new Observable.OnSubscribe<Long>() {
            @Override
            public void call(Subscriber<? super Long> subscriber) {
                mDBHelper.insertOrReplace(mDBHelper.getPostInfoDao(),obj);
            }
        });
    }

    @Override
    public Observable<Void> insertOrReplace(final Iterable<PostInfo> entities) {
        return Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                mDBHelper.insertOrReplaceInTx(mDBHelper.getPostInfoDao(),entities);
            }
        });
    }
}
