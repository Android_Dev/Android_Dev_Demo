package com.android.dev.data.api.model;

import com.android.dev.data.db.PostInfo;

import java.util.List;

/**
 * Created by wangyouguo on 16/1/14.
 */
public class PostInfoResp {
  public  boolean error;
   public List<PostInfo> results;

    @Override
    public String toString() {
        return "PostInfoResp{" +
                "error=" + error +
                ", results=" + results +
                '}';
    }
}
