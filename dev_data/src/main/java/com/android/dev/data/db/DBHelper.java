package com.android.dev.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.query.QueryBuilder;
import rx.Observable;
import rx.Subscriber;

/**
 * Created with Android Studio.
 * Project:Android_Dev
 * Title:DBHelper
 * Description:数据库操作接口
 * Copyright:Copyright (c) 2014
 * Company:中国平安健康保险有限公司
 * Author:mahuiyang
 * Date:16/1/11
 * Time:13:22
 * Version 1.0
 */
@Singleton
public class DBHelper {

    private final DaoSession mDaoSession;

    private final SQLiteDatabase mDb;

    private static final String TAG = "DBHelper";

    @Inject
    public DBHelper (Context context) {
        DaoMaster.OpenHelper helper = new DaoMaster.DevOpenHelper(context,"gank.db",null);
        mDb = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(mDb);
        mDaoSession = daoMaster.newSession();
    }

    /**
     * 获取postInfo表的dao操作对象
     * @return null or PostInfoDao object
     */
    public PostInfoDao getPostInfoDao() {
        return null == mDaoSession ? null : mDaoSession.getPostInfoDao();
    }

    /**
     * 分页查询指定表的指定数据
     * @param dao 表dao
     * @param index 分页索引
     * @param size  分页size
     * @param <T>   表数据模型
     * @return
     */
    public <T> List<T> query (AbstractDao dao, int index, int size) {
        if (null == dao) {
            throw new IllegalArgumentException("query()---> AbstractDao object may not be null!");
        }
        QueryBuilder<T> builder = dao.queryBuilder();
        builder.offset(index * size);
        builder.limit(size);
        return builder.list();
    }

    /**
     * 查询指定ID的数据
     * @param dao
     * @param id 数据ID
     * @return
     */
    public Object load (AbstractDao dao, Object id) {
        if (null == dao) {
            throw new IllegalArgumentException("query()---> AbstractDao object may not be null!");
        }
        return dao.load(id);
    }


    /**
     * 插入或者更新一条数据
     * @param dao   表dao
     * @param data  数据
     * @return
     */
    public <T extends Object> long insertOrReplace (AbstractDao dao, T data) {
        if (null == dao) {
            throw new IllegalArgumentException("insertOrReplace()---> AbstractDao object may not be null!");
        }
        return dao.insertOrReplace(data);
    }

    /**
     * 批量插入或者更新数据
     * @param dao   表dao
     * @param entities  数据列表
     * @param <T>   数据对象模型
     */
    public <T> void insertOrReplaceInTx (AbstractDao dao,Iterable<T> entities) {
        if (null == dao) {
            throw new IllegalArgumentException("insertOrReplaceInTx()---> AbstractDao object may not be null!");
        }
        if (null == entities) {
            return;
        }
        dao.insertOrReplaceInTx(entities);
    }

    /**
     * 删除指定主键的一条数据
     * @param dao
     * @param key
     */
    public <T extends Object> void deleteByKey (AbstractDao dao,T key) {
        if (null == dao) {
            throw new IllegalArgumentException("deleteByKey()---> AbstractDao object may not be null!");
        }
        dao.deleteByKey(key);
    }

    /**
     * 批量删除指定主键的数据
     * @param dao
     * @param keys
     */
    public <T> void deleteByKeyInTx (AbstractDao dao,Iterable<T> keys) {
        if (null == dao) {
            throw new IllegalArgumentException("deleteByKeyInTx()---> AbstractDao object may not be null!");
        }
        dao.deleteByKeyInTx(keys);
    }

    public <T> Observable<T> makeObservable (final Callable<T> fun) {
        return Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                try {
                    subscriber.onNext(fun.call());
                } catch (Exception e) {
                    Log.e(TAG, "makeObservable()---> Error reading from the database", e);
                }
            }
        });
    }
}
