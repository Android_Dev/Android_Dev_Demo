package com.android.dev.data.db;

import rx.Observable;

/**
 * Created with Android Studio.
 * Project:Android_Dev
 * Title:IDBUpdater
 * Description:数据库更新操作接口
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/11
 * Time:16:58
 * Version 1.0
 */
public interface IDBUpdater {

    /**
     * 更新一条帖子
     * @param info
     * @return
     */
    Observable<Long> insertOrReplace (PostInfo info);

    /**
     * 批量更新帖子
     * @param entities
     * @return
     */
    Observable<Void>  insertOrReplace (Iterable<PostInfo> entities);
}
