package com.android.dev.data.di.component;

import android.app.Activity;
import android.content.Context;

import com.android.dev.data.BaseActivity;
import com.android.dev.data.di.module.ActivityModule;
import com.android.dev.data.di.qualifier.ActivityContext;
import com.android.dev.data.di.scope.PerActivity;

import dagger.Component;

/**
 * Created by wangyouguo on 16/1/12.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface ActivityComponent {
    void inject(BaseActivity activity);

    Activity activity();

    @ActivityContext
    Context context();
}
