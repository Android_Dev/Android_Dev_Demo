package com.android.dev.data;

import android.app.Application;

import com.android.dev.data.di.DataModule;
import com.android.dev.data.di.component.ApplicationComponent;
import com.android.dev.data.di.component.DaggerApplicationComponent;
import com.android.dev.data.di.module.ApplicationModule;

/**
 * Created by wangyouguo on 16/1/18.
 */
public class BaseApplication extends Application {

    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder().
                applicationModule(new ApplicationModule(this)).dataModule(new DataModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
