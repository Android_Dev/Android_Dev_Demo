package com.android.dev.data.db;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;

/**
 * Created with Android Studio.
 * Project:Android_Dev
 * Title:DBDeletorImpl
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/11
 * Time:16:55
 * Version 1.0
 */
public class DBDeletorImpl implements IDBDeletor {

    @Inject
    DBHelper mDBHelper;

    @Override
    public Observable<Void> deletePostInfo(final long key) {
        return Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                mDBHelper.deleteByKey(mDBHelper.getPostInfoDao(),key);
            }
        });
    }

    @Override
    public Observable<Void> delete(final Iterable<PostInfo> keys) {
        return Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                mDBHelper.deleteByKeyInTx(mDBHelper.getPostInfoDao(),keys);
            }
        });
    }
}
