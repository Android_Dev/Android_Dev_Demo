package com.android.dev.data.di.module;

import android.app.Activity;
import android.content.Context;

import com.android.dev.data.di.qualifier.ActivityContext;

import dagger.Module;
import dagger.Provides;

/**
 * Created by wangyouguo on 16/1/12.
 */
@Module
public class ActivityModule {
    private Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    Activity provideActivity() {
        return mActivity;
    }

    @Provides
    @ActivityContext
    Context providesContext() {
        return mActivity;
    }
}
