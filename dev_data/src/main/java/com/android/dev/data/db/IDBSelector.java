package com.android.dev.data.db;

import java.util.List;

import rx.Observable;

/**
 * Created with Android Studio.
 * Project:Android_Dev
 * Title:IDBSelector
 * Description:数据库查询操作接口
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/11
 * Time:16:18
 * Version 1.0
 */
public interface IDBSelector<T> {

    /**
     * 分页查询
     * @param index
     * @param size
     * @return
     */
    Observable<List<T>> query (int index, int size);

    /**
     * 查询指定主键的数据
     * @param id
     * @return
     */
    Observable<T> query (long id);
}
