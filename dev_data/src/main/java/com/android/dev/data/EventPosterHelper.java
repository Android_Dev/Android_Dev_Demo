package com.android.dev.data;

import android.os.Handler;
import android.os.Looper;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;

public class EventPosterHelper {

    private final EventBus mEventBus;

    @Inject
    public EventPosterHelper(EventBus bus) {
        mEventBus = bus;
    }

    /**
     * Helper method to post an event from a different thread to the main one.
     */
    public void postEventSafely(final Object event) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mEventBus.post(event);
            }
        });
    }
}