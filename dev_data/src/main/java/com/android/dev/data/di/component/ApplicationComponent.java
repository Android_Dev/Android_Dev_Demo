package com.android.dev.data.di.component;

import android.content.Context;

import com.android.dev.data.DataManager;
import com.android.dev.data.db.DBHelper;
import com.android.dev.data.di.DataModule;
import com.android.dev.data.di.module.ApplicationModule;
import com.android.dev.data.di.qualifier.ApplicationContext;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import de.greenrobot.event.EventBus;
import retrofit2.Retrofit;

/**
 * Created by wangyouguo on 16/1/18.
 */
@Singleton
@Component(modules = {ApplicationModule.class, DataModule.class})
public interface ApplicationComponent {
    @ApplicationContext
    Context context();

    DBHelper dbHelper();

    Retrofit retrofit();

    DataManager dataManager();

    EventBus bus();

    Gson gson();
}
