package com.android.dev.data.api.service;

import com.android.dev.data.api.model.PostInfoResp;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by wangyouguo on 16/1/12.
 */
public interface ApiMainService {

    @GET("data/{type}/{pageSize}/{pageIndex}")
    Observable<PostInfoResp> getCategoryDataList(
            @Path("type")
            String type,
            @Path("pageSize")
            int pageSize,
            @Path("pageIndex")
            int pageIndex
    );

    @GET("random/data/{type}/{pageSize}")
    Observable<PostInfoResp> getRandomCategoryDataList(
            @Path("type")
            String type,
            @Path("pageSize")
            int pageSize
    );

}
