package com.android.dev.data.api.service;

import com.android.dev.data.api.model.PostInfoResp;

import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by wangyouguo on 16/1/15.
 */
public interface ApiListService {
    @POST("day/{year}/{month}/{day}")
    Observable<PostInfoResp> getDayData(
            @Path("year")
            int year,
            @Path("month")
            int month,
            @Path("day")
            int day
    );

}
