package com.android.dev.data.db;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

/**
 * GreenDao自动生成数据库表的入口
 */
public class DaoGenerator {

    public static void main (String[] args) {
        String pkg = DaoGenerator.class.getPackage().getName();
        Schema schema = new Schema(1000, pkg);
        createPostTable(schema);
        try {
            new de.greenrobot.daogenerator.DaoGenerator().generateAll(schema,
                    "/Users/mahuiyang/DevTools/StudioProjects/Android_Dev/dev_data/src/main/java-gen");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  创建帖子信息表
     * @param schema
     */
    private static void createPostTable (Schema schema) {
        Entity entity = schema.addEntity("PostInfo");
        entity.addIdProperty();//帖子数据库id
        entity.addStringProperty("who");//帖子标题
        entity.addLongProperty("published_at");//发表时间
        entity.addStringProperty("desc");//帖子描述
        entity.addStringProperty("type");//类型
        entity.addStringProperty("url");//
        entity.addBooleanProperty("used");//
        entity.addStringProperty("post_id");//帖子id
        entity.addLongProperty("created_at");//创建时间
        entity.addLongProperty("updated_at");//更新时间
    }

}
