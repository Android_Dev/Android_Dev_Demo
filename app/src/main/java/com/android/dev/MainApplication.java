package com.android.dev;

import android.app.Application;

import com.android.dev.data.BaseApplication;
import com.android.dev.data.di.DataModule;
import com.android.dev.data.di.component.ApplicationComponent;
import com.android.dev.data.di.component.DaggerApplicationComponent;
import com.android.dev.data.di.module.ApplicationModule;

/**
 * Created by wangyouguo on 16/1/18.
 */
public class MainApplication extends BaseApplication {
    @Override
    public void onCreate() {
        super.onCreate();

    }
}
