package com.android.dev;

import android.util.Log;

import com.android.dev.data.DataManager;
import com.android.dev.data.api.model.PostInfoResp;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.Subject;

/**
 * Created by wangyouguo on 16/1/12.
 */
public class DataPresenter {
    DataManager mDataManager;

    @Inject
    DataPresenter(DataManager dataManager) {
        Log.e("wyg", "DataPresenter=start==>");
        mDataManager = dataManager;
    }

    public void getCategoryDataList(final String type, final int pageSize, final int pageIndex) {
        mDataManager.getCategoryDataList(type, pageSize, pageIndex)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<PostInfoResp>() {
                    @Override
                    public void call(PostInfoResp postInfoResp) {
                        Log.e("wyg", "getCategoryDataList=start==>" + postInfoResp);
                    }
                });
    }

    public void getRandomCategoryDataList(String type, int pageSize) {
        mDataManager.getRandomCategoryDataList(type, pageSize)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PostInfoResp>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(PostInfoResp postInfoResp) {

                    }
                });
    }

    public void getDayData(int year, int month, int day) {
        mDataManager.getDayData(year, month, day)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PostInfoResp>() {
                    @Override
                    public void onCompleted() {
                        Log.e("wyg", "onCompleted--->>>" );
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("wyg", "onError--->>>" + e);
                    }

                    @Override
                    public void onNext(PostInfoResp postInfoResp) {
                        Log.e("wyg", "getDayData--->>>" + postInfoResp);
                    }
                });
    }

}
