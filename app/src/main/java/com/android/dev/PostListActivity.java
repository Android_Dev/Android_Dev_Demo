package com.android.dev;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.android.dev.base.di.HasComponent;
import com.android.dev.data.BaseActivity;
import com.android.dev.data.di.component.ApplicationComponent;
import com.android.dev.data.di.module.ActivityModule;
import com.android.dev.event.PostDetailFragmentEvent;
import com.android.dev.presenter.PostDetailFragment;
import com.android.dev.presenter.PostListFragment;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;

/**
 * Created with Android Studio.
 * Project:Android_Dev_Demo
 * Title:PostListActivity
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/27
 * Time:13:32
 * Version 1.0
 */
public class PostListActivity extends BaseActivity implements HasComponent<MainActivityComponent>{

    private MainActivityComponent mainActivityComponent;

    @Inject
    EventBus mEventBus;

    private static final String TAG = PostListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_frag);

        //Dagger2 依赖注入初始化
        ApplicationComponent applicationComponent = ((MainApplication)getApplication()).getApplicationComponent();
        mainActivityComponent = DaggerMainActivityComponent.builder().applicationComponent(applicationComponent)
                .activityModule(new ActivityModule(this)).build();
        mainActivityComponent.inject(this);

        //监听Back事件,并在fragment堆栈为空时,退出当前activity
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                    finish();
                }
            }
        });

        addNewFragment(PostListFragment.newInstance());
        //注册eventBus
        mEventBus.register(this);
    }

    @Override
    protected void onDestroy() {
        mainActivityComponent = null;
        //销毁eventBus
        mEventBus.unregister(this);
        super.onDestroy();
    }

    @Override
    public MainActivityComponent getComponent() {
        return mainActivityComponent;
    }

    /**
     * 添加一个fragment到当前activity
     * @param fragment
     */
    private void addNewFragment (Fragment fragment) {
        if (null == fragment) {
            throw new IllegalArgumentException("addNewFragment()---> fragment object maybe not null!");
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Log.d(TAG, "addNewFragment()--->fragment=" + fragment);
        transaction.add(R.id.fr_layout, fragment);
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commitAllowingStateLoss();
    }

    //点击帖子列表item跳转时,更新fragment
    @Subscribe(threadMode = ThreadMode.MainThread)//EventBus3.0用法
    public void switchPostDetailFragment (PostDetailFragmentEvent eventMsg) {
        Log.d(TAG,"switchPostDetailFragment()--->eventMsg...");
        PostDetailFragment fragment = PostDetailFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putLong("id", eventMsg.getPostId());
        fragment.setArguments(bundle);
        addNewFragment(fragment);
    }
}
