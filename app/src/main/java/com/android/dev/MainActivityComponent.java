package com.android.dev;

import com.android.dev.data.di.component.ActivityComponent;
import com.android.dev.data.di.component.ApplicationComponent;
import com.android.dev.data.di.module.ActivityModule;
import com.android.dev.data.di.scope.PerActivity;
import com.android.dev.delegate.PostListDelegate;
import com.android.dev.presenter.PostDetailFragment;
import com.android.dev.presenter.PostListFragment;

import dagger.Component;

/**
 * Created by wangyouguo on 16/1/18.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface MainActivityComponent extends ActivityComponent {

    void inject(MainActivity activity);
    void inject(PostListActivity activity);

    void inject (PostListFragment fragment);

    void inject (PostListDelegate delegate);

    void inject (PostDetailFragment fragment);
}
