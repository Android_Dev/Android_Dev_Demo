package com.android.dev.event;

/**
 * Created with Android Studio.
 * Project:Android_Dev_Demo
 * Title:PostDetailFragmentEvent
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/27
 * Time:19:04
 * Version 1.0
 */
public class PostDetailFragmentEvent {

    private long postId;

    public PostDetailFragmentEvent(long postId) {
        this.postId = postId;
    }

    public long getPostId() {
        return postId;
    }
}
