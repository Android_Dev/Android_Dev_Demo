package com.android.dev.presenter;

import android.os.Bundle;
import android.util.Log;

import com.android.dev.MainActivityComponent;
import com.android.dev.base.di.ComponentFinder;
import com.android.dev.base.presenter.FragmentPresenter;
import com.android.dev.data.db.DBSelectorImpl;
import com.android.dev.data.db.PostInfo;
import com.android.dev.delegate.PostListDelegate;

import java.util.List;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created with Android Studio.
 * Project:Android_Dev
 * Title:PostListFragment
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/12
 * Time:11:03
 * Version 1.0
 */
public class PostListFragment extends FragmentPresenter<PostListDelegate> {

    @Inject
    DBSelectorImpl dbSelector;

    private MainActivityComponent mainActivityComponent;

    private static final String TAG = PostListFragment.class.getSimpleName();

    public static PostListFragment newInstance () {
        PostListFragment fragment = new PostListFragment();
        return fragment;
    }

    @Override
    protected Class getDelegateClass() {
        return PostListDelegate.class;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivityComponent = ComponentFinder.findActivityComponent(getActivity());
        mainActivityComponent.inject(this);
        mainActivityComponent.inject(mViewDelegate);

        mViewDelegate.setContext(getActivity());
        loadList();
    }

    @Override
    public void onDestroy() {
        mViewDelegate = null;
        mainActivityComponent = null;
        super.onDestroy();
    }

    private void loadList () {
        dbSelector.query(0,10)
                .subscribeOn(Schedulers.io())//观察者代码运行的线程(调度器)
                .observeOn(AndroidSchedulers.mainThread())//订阅者运行的线程
                .subscribe(new Action1<List<PostInfo>>() {
            @Override
            public void call(List<PostInfo> postInfos) {
                Log.d(TAG,"loadList()---> call... size=" + (null == postInfos ? 0 : postInfos.size()));
                //updateUI
                mViewDelegate.setList(postInfos);
            }
        });
    }
}
