package com.android.dev.presenter;

import android.os.Bundle;

import com.android.dev.MainActivityComponent;
import com.android.dev.base.di.ComponentFinder;
import com.android.dev.base.presenter.FragmentPresenter;
import com.android.dev.data.db.DBSelectorImpl;
import com.android.dev.data.db.PostInfo;
import com.android.dev.delegate.PostInfoDelegate;
import com.google.gson.Gson;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created with Android Studio.
 * Project:Android_Dev_Demo
 * Title:PostDetailFragment
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/27
 * Time:18:23
 * Version 1.0
 */
public class PostDetailFragment extends FragmentPresenter<PostInfoDelegate> {

    @Inject
    DBSelectorImpl mDBSelectorImpl;
    @Inject
    Gson mGson;

    private MainActivityComponent mMainActivityComponent;

    @Override
    protected Class<PostInfoDelegate> getDelegateClass() {
        return PostInfoDelegate.class;
    }

    public static PostDetailFragment newInstance () {
        PostDetailFragment fragment = new PostDetailFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMainActivityComponent = ComponentFinder.findActivityComponent(getActivity());
        mMainActivityComponent.inject(this);

        loadPostInfo();
    }

    @Override
    public void onDestroy() {
        mMainActivityComponent = null;
        mViewDelegate = null;
        super.onDestroy();
    }

    private void loadPostInfo () {
        Bundle bundle = getArguments();
        long id = bundle.getLong("id");
        mDBSelectorImpl.query(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<PostInfo>() {
                    @Override
                    public void call(PostInfo postInfo) {
                        mViewDelegate.updateContent(mGson.toJson(postInfo));
                    }
                });
    }
}
