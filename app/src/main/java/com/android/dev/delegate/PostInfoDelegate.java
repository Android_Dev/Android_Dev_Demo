package com.android.dev.delegate;

import android.widget.TextView;

import com.android.dev.R;

/**
 * Created with Android Studio.
 * Project:Android_Dev_Demo
 * Title:PostInfoDelegate
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/27
 * Time:18:24
 * Version 1.0
 */
public class PostInfoDelegate extends AppDelegate {

    @Override
    protected int getRootLayoutId() {
        return R.layout.frag_post_detail;
    }

    @Override
    public void initWidget() {

    }

    public void updateContent(String json) {
        TextView textView = get(R.id.tv_detail);
        textView.setText(json);
    }
}
