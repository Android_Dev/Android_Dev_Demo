package com.android.dev.delegate;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.dev.R;
import com.android.dev.data.db.PostInfo;
import com.android.dev.event.PostDetailFragmentEvent;

import java.util.List;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;

/**
 * Created with Android Studio.
 * Project:Android_Dev_Demo
 * Title:PostListDelegate
 * Description:
 * Copyright:Copyright (c) 2014
 * Company:平安健康互联网股份有限公司上海分公司
 * Author:mahuiyang
 * Date:16/1/26
 * Time:10:12
 * Version 1.0
 */
public class PostListDelegate extends AppDelegate {

    private Context mCtx;
    private PostInfoAdapter mAdapter;
    @Inject
    EventBus mEventBus;

    private static final String TAG = PostListDelegate.class.getSimpleName();

    @Override
    protected int getRootLayoutId() {
         return R.layout.frag_post_list;
    }

    @Override
    public void initWidget() {
        mAdapter = new PostInfoAdapter(mCtx);
        RecyclerView recyclerView = get(R.id.list_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mCtx));
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                PostInfo info = mAdapter.getPostInfo(position);
                mEventBus.post(new PostDetailFragmentEvent(info.getId()));
            }
        });
    }

    public void setContext(Context context) {
        this.mCtx = context;
    }

    public void setList (List<PostInfo> list) {
        mAdapter.setPostInfos(list);
        mAdapter.notifyDataSetChanged();
    }


    public class PostInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final LayoutInflater mLayoutInflater;
        private List<PostInfo> mPostInfos;
        private OnItemClickListener mOnItemClickListener;
        public PostInfoAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new PostInfoViewHolder(mLayoutInflater.inflate(R.layout.post_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            PostInfo postInfo = mPostInfos.get(position);
            if (holder instanceof PostInfoViewHolder) {
                ((PostInfoViewHolder) holder).tvTitle.setText(postInfo.getWho());
                ((PostInfoViewHolder) holder).tvDesc.setText(postInfo.getDesc());
            }
        }

        @Override
        public int getItemCount() {
            return null == mPostInfos ? 0 : mPostInfos.size();
        }

        public void setPostInfos(List<PostInfo> postInfos) {
            this.mPostInfos = postInfos;
        }

        public PostInfo getPostInfo (int position) {
            int size = null == mPostInfos ? 0 : mPostInfos.size();
            if (size > position) {
                return mPostInfos.get(position);
            }
            return null;
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.mOnItemClickListener = onItemClickListener;
        }

        class PostInfoViewHolder extends RecyclerView.ViewHolder {

            public TextView tvTitle;
            public TextView tvDesc;

            public PostInfoViewHolder(View itemView) {
                super(itemView);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mOnItemClickListener) {
                            mOnItemClickListener.onItemClick(v,getAdapterPosition());
                        }
                    }
                });

                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                tvDesc = (TextView) itemView.findViewById(R.id.tv_desc);
            }
        }
    }

    interface OnItemClickListener {
        void onItemClick (View view, int position);
    }
}
